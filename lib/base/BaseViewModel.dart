import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class BaseViewModel extends ChangeNotifier {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  onshowSnackBar({int timer, String title}) {
    return scaffoldKey.currentState.showSnackBar(SnackBar(
      duration: Duration(seconds: timer),
      backgroundColor: Color(0xff2A2E43),
      content: Text(
        title,
        style: TextStyle(
          fontFamily: "kofi",
          fontSize: 18,
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
      ),
    ));
  }

//  Future<File> sendImageCamera(ImageSource source) async {
//    var image = await ImagePicker.pickImage(
//        source: source, maxWidth: 800, maxHeight: 800);
//    return image;
//  }

  Size screenSize(BuildContext context) {
    return MediaQuery.of(context).size;
  }

  double screenHeight(BuildContext context, {double dividedBy = 1}) {
    return screenSize(context).height / dividedBy;
  }

  double screenWidth(BuildContext context, {double dividedBy = 1}) {
    return screenSize(context).width / dividedBy;
  }
}
