import 'package:flutter_app/views/aws_view/AwsViewModel.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

void setupLocator() async {
  locator.registerFactory(() => AwsViewModel());

//  locator.registerLazySingleton(() => LocalizationService());
}
