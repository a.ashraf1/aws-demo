import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/base/locator.dart';
import 'package:flutter_app/views/aws_view/AwsView.dart';

List<CameraDescription> cameras = [];

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();

  final cameras = await availableCameras();

  final firstCamera = cameras.first;

  runApp(MyApp(firstCamera));
}

class MyApp extends StatelessWidget {
  final firstCamera;

  MyApp(this.firstCamera);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'AWS',
      home: AwsView(
        camera: firstCamera,
      ),
    );
  }
}
