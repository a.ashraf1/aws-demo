class LabelObjectsModel {
  String labelModelVersion;
  List<_LabelsListBean> labels;

  LabelObjectsModel({this.labelModelVersion, this.labels});

  LabelObjectsModel.fromJson(Map<String, dynamic> json) {
    this.labelModelVersion = json['LabelModelVersion'];
    this.labels = (json['Labels'] as List)!=null?(json['Labels'] as List).map((i) => _LabelsListBean.fromJson(i)).toList():null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['LabelModelVersion'] = this.labelModelVersion;
    data['Labels'] = this.labels != null?this.labels.map((i) => i.toJson()).toList():null;
    return data;
  }

}

class _LabelsListBean {
  String name;
  double confidence;
  List<String> instances;
  List<String> parents;

  _LabelsListBean({this.name, this.confidence, this.instances, this.parents});

  _LabelsListBean.fromJson(Map<String, dynamic> json) {
    this.name = json['Name'];
    this.confidence = json['Confidence'];

    List<dynamic> instancesList = json['Instances'];
    this.instances = new List();
    this.instances.addAll(instancesList.map((o) => o.toString()));

    List<dynamic> parentsList = json['Parents'];
    this.parents = new List();
    this.parents.addAll(parentsList.map((o) => o.toString()));
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Name'] = this.name;
    data['Confidence'] = this.confidence;
    data['Instances'] = this.instances;
    data['Parents'] = this.parents;
    return data;
  }
}
