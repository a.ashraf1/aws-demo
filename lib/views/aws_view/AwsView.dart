import 'package:flutter/material.dart';
import 'package:flutter_app/base/BaseView.dart';
import 'package:camera/camera.dart';
import 'package:flutter_app/views/aws_view/AwsViewModel.dart';

class AwsView extends StatelessWidget {
  final CameraDescription camera;

  const AwsView({Key key, this.camera}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseView<AwsViewModel>(onModelReady: (model) {
      model.controller = CameraController(
        camera,
        ResolutionPreset.medium,
      );
      model.initializeControllerFuture = model.controller.initialize();
      model.onInItTensflow();

    }, onModelDispose: (model) {
      model.controller.dispose();
    }, builder: (mContext, model, child) {
      Size size = MediaQuery.of(context).size;
      List<Widget> stackChildren = [];

      stackChildren.add(
        buildContainer(context, model),
      );
      stackChildren.add(
        model.isFinished ? rekogonitonData(model) : Container(),
      );
      stackChildren.add(
        model.isDetecting ? detectBox(size, model) : Container(),
      );

      return Scaffold(
        floatingActionButton: FloatingActionButton(
          child: GestureDetector(
            child: Icon(Icons.camera_alt),
          ),
          onPressed: () {
            model.onFinalStream();
          },
        ),
        body: SafeArea(
          child: Stack(
            children: stackChildren,
          ),
        ),
      );
    });
  }

  Container buildContainer(BuildContext context, AwsViewModel model) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: double.infinity,
      child: FutureBuilder<void>(
        future: model.initializeControllerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return CameraPreview(model.controller);
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }

  Positioned rekogonitonData(AwsViewModel model) {
    return Positioned(
      left: 5,
      right: 5,
      bottom: 50.0,
      child: Container(
        margin: EdgeInsets.all(5),
        padding: EdgeInsets.all(5),
        decoration: BoxDecoration(color: Colors.black.withOpacity(0.5)),
        alignment: Alignment.center,
        child: Text(
          "${model.resultName_1} - ${model.resultName_3} - ${model.resultName_2}",
          style: TextStyle(
              fontSize: 20, color: Colors.white, fontWeight: FontWeight.w900),
        ),
      ),
    );
  }

  Widget detectBox(Size screen, AwsViewModel model) {
    if (model.xrecognitions == null) return Container();
    if (model.ximageWidth == null || model.ximageHeight == null)
      return Container();

    double factorX = screen.width;
    double factorY = model.ximageHeight / model.ximageHeight * screen.width;

    Color blue = Colors.red;
    return Positioned(
      left: model.xrecognitions[0]["rect"]["x"] * factorX,
      top: model.xrecognitions[0]["rect"]["y"] * factorY,
      width: model.xrecognitions[0]["rect"]["w"] * factorX,
      height: model.xrecognitions[0]["rect"]["h"] * factorY,
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
            color: blue,
            width: 3,
          ),
        ),
      ),
    );
//    return model.xrecognitions.map((re) {
//      return Positioned(
//        left: re["rect"]["x"] * factorX,
//        top: re["rect"]["y"] * factorY,
//        width: re["rect"]["w"] * factorX,
//        height: re["rect"]["h"] * factorY,
//        child: Container(
//          decoration: BoxDecoration(
//            border: Border.all(
//              color: blue,
//              width: 3,
//            ),
//          ),
////          child: Text(
////            "${re["detectedClass"]} ${(re["confidenceInClass"] * 100).toStringAsFixed(0)}%",
////            style: TextStyle(
////              background: Paint()..color = blue,
////              color: Colors.white,
////              fontSize: 15,
////            ),
////          ),
//        ),
//      );
//    }).toList();
  }
}
