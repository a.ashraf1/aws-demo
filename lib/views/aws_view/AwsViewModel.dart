import 'dart:convert';
import 'dart:io';
import 'package:aws_ai/aws_ai.dart';
import 'package:camera/camera.dart';
import 'package:flutter_app/model/LabelObjectsModel.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';
import 'package:flutter_app/base/BaseViewModel.dart';

//import 'package:flutter_tts/flutter_tts.dart';
import 'package:flutter/services.dart';
import 'package:tflite/tflite.dart';

class AwsViewModel extends BaseViewModel {
  //Access Keys
  String accessKey = "AKIAIQLNTYY3WEIWNNQQ",
      secretKey = "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",
      region = "eu-central-1";

  //RekogntionStep
  var xResults;

  Future<void> onAmazonRekogntion(String xPath) async {
    RekognitionHandler rekognition =
        RekognitionHandler(accessKey, secretKey, region);
    await rekognition.detectLabels(File(xPath)).then((detectedImage) {
      xResults = detectedImage;
    });
    notifyListeners();
    onReturnData();
  }

  // return data from amazon rekogontion
  String resultName_1;
  String resultName_2;
  String resultName_3;
  bool isFinished = false;

  onReturnData() {
    var deco = jsonDecode(xResults);
    LabelObjectsModel xFaceDet = LabelObjectsModel.fromJson(deco);
    resultName_1 = xFaceDet.labels[0].name;
    resultName_3 = xFaceDet.labels[1].name;
    resultName_2 = xFaceDet.labels[2].name;
    var total = resultName_1 + resultName_3 + resultName_2;
//    _speak(total);
    isFinished = true;

    notifyListeners();
    onRemoveItem();
  }

  // remove amazon data on view
  onRemoveItem() {
    Future.delayed(Duration(seconds: 3), () {
      isFinished = false;
      notifyListeners();
    });
  }

  // take photo with camera
  CameraController controller;
  Future<void> initializeControllerFuture;

  Future<void> onCameraTakePhoto() async {
    try {
      final path = join(
        (await getTemporaryDirectory()).path,
        '${DateTime.now()}.png',
      );
      await initializeControllerFuture;
      await controller.takePicture(path);
      await onAmazonRekogntion(path);
    } catch (e) {
      print(e);
    }
    notifyListeners();
  }

  // inint tensflow
  bool xbusy = false;

  onInItTensflow() {
    xbusy = true;

    loadModel().then((val) {
      xbusy = false;
      notifyListeners();
    });
  }

  loadModel() async {
    Tflite.close();
    try {
      await Tflite.loadModel(
        model: "assets/tflite/ssd_mobilenet.tflite",
        labels: "assets/tflite/ssd_mobilenet.txt",
      );
    } on PlatformException {
      print("Failed to load the model");
    }
  }

  double ximageWidth;
  double ximageHeight;
  List xrecognitions;
  bool isDetecting = false;

  onFinalStream() async {
    // start streaming
    // detect true
    // take photo
    // send to amazon
    // check detect if true repeat function
    await controller.startImageStream(
      (CameraImage img) async {
        // set heigh and width for current objects
        if (!isDetecting) {
          ximageHeight = img.height.toDouble();
          ximageWidth = img.width.toDouble();
          isDetecting = true;
          notifyListeners();
          Future.delayed(Duration(seconds: 2), () {
            // stop Stream
            return controller.stopImageStream();
          }).then((_) {
            //start detect
            Tflite.detectObjectOnFrame(
              bytesList: img.planes.map(
                (plane) {
                  return plane.bytes;
                },
              ).toList(),
              imageHeight: img.height,
              imageWidth: img.width,
            ).then((value) {
              isDetecting = false;
              xrecognitions = value;
              print(value);
              notifyListeners();
            });
          }).then((_) {
            // take photo
            return onCameraTakePhoto();
          }).then((_) {
            // repeat             // stop Stream
            return onFinalStream();
          });
        }
      },
    );
  }

//  FlutterTts xFlutterSpeak = FlutterTts();
//
//  Future _speak(String text) async {
//    xFlutterSpeak.speak(text);
//  }
}
